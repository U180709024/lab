public class FindLetterGrade{

	public static void main(String[] args){
		int score = Integer.parseInt(args[0]);

        if(0<=score && score<60){
            System.out.println("F");
        }else if(60<=score && score<70){
            System.out.println("D");
        }else if(70<=score && score<80){
            System.out.println("C");
        }else if(80<=score && score<90){
            System.out.println("B");
        }else{
            System.out.println("A");
        }
    }
}

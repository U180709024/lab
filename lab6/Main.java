public class Main{
    public static void main(String[] args) {

        Point p = new Point(4,6);
        Rectangle r = new Rectangle(p,5,9);
       
        System.out.println("Area of rectangle: "+r.area());
        System.out.println("Perimeter of rectangle: "+r.perimeter());

        Point[] points = r.corners();
        for (int i=0; i<points.length; i++){
            System.out.println("Point "+(i+1)+": "+points[i].xCoord + "," + points[i].yCoord);

        }

        Circle c = new Circle(p,10);
        System.out.println("-------------------");
        System.out.println("Area of circle: "+c.area());
        System.out.println("Perimeter of circle: "+c.perimeter());

        Point p2 = new Point(4,20);
        Circle c2 = new Circle(p2,5);

        System.out.println("Circle intersects with cirlce 2 (c2): "+c.intersect(c2));





    }
}